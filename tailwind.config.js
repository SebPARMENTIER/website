module.exports = {
  purge: [
    './index.html',
    './src/**/*.vue',
    './src/**/*.js',
  ],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
