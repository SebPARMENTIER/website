# adacraft website

This project is part of the [adacraft](https://www.adacraft.org/), and
implements the frontend part of the portal, i.e. where people manage their
profile and projects.

The adacraft portal is a static website developped in Vue 3 and Tailwind CSS. It
connects to a Fauna database to manage user and project data, and an Object
Storage container (S3 compatible) to manage file storage.

For more information about adacraft and its developement, go to the [main
project page](https://gitlab.com/adacraft/main).

## Instructions

We use `pnpm` as our package manager.

To initilaize your working directory:

```
$ pnpm install
```

To launch the portal during development:

```
$ pnpm run dev
```

To build the portal for deployement:

```
$ pnpm run build
```

## Environment settings

### Environment variables

Before running the portal in dev mode or building it for any environment you
need to specify the auth URL, the Fauna DB and the S3 storage to use. This is
done by setting the following environments variables:

```
VITE_GOTRUE_API_URL=https://some.adacraft.env.as.netlify.app/.netlify/identity
VITE_FAUNA_PUBLIC_KEY=a_fauna_key_with_public_readonly_access
VITE_ADACRAFT_STORAGE_URL=https://the.s3.storage.endpoint.org
```

You can put these variables in a dot env file. Use file name `.env.local` so
that it is ignored by git and won't be commit by error.

### Settings for production

Here are the values for the production environment:

```
VITE_GOTRUE_API_URL=https://adacraft.netlify.app/.netlify/identity
VITE_ADACRAFT_STORAGE_URL=https://storage.beta.adacraft.org
VITE_FAUNA_PUBLIC_KEY=fnADx7iDttACBzcebwd3q6J8AaynXYEyT8SbZZ7F
```

It's totally fine if you use these, even for testing purposes, and usually it's
what people want to use.

For now, there is no default testing environment. If you need one, you have to
set a Fauna database, an object storage sapce, and a Netlify Identity endpoint,
and make the three work together. Get in touch with the adacraft team for
instructions.

The only risk with using the production environment for testing, is to mess up
your adacraft account (user profile or projects). But, as all write operations
expect that you're logged in, you won't be able to modify other people data. At
worst, the data associated to your account will be corrupted. To avoid
undersirable effects, we recommand that you create a specific adacraft account
for your testing.