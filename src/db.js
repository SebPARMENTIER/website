import faunadb from 'faunadb'
import { faunaPublicKey } from './config'

// Basic types for adacraft

/**
 * @typedef {{
 *   id?: string,
 *   user_metadata?: {
 *     full_name: string
 *   },
 *   username?: string
 *   avatar?: string
 *   bio?: string
 * }} User
 * 
 * @typedef {{
 *   name?: string,
 *   id?: string,
 *   image?: string,
 *   user?: string,
 *   userId?: string,
 *   userName?: string,
 *   abstract?: string,
 *   owner?: faunadb.Ref,
 *   trash?: boolean,
 *   thrash?: boolean,
 *   storage?: string
 * }} Project
 * 
 * @typedef {{main: [string]}} FeaturedProjects
 */


// FaunaDB types for adacraft

/**
 * @typedef {faunadb.values.Document<User>} DbUser
 * @typedef {faunadb.values.Document<Project>} DbProject
 */

const q = faunadb.query

const publicClient = new faunadb.Client({
  secret: faunaPublicKey
})

/**
 * @param {string} token 
 */
const getClient = (token) => (
  new faunadb.Client({ secret: token })
)

/**
 * @param {faunadb.Client} client 
 * @returns {Promise<DbUser>}
 */
const getCurrentUser = async function (client) {
  // TODO catch fauna exceptions (like unreachable, etc.)
  /** @type any */
  const identity = await client.query(q.CurrentIdentity())
  /** @type string */
  const userId = identity.value.id
  // TODO catch fauna exceptions (like unreachable, etc.)
  return await client.query(
    q.Get(q.Ref(q.Collection('users'), userId))
  )
}

const getFeaturedProjects = async function () {
  // We know that the featured projects are public resources.
  const client = publicClient
  /** @type {faunadb.values.Document<FeaturedProjects>} */
  const response =
    await client
      .query(
        // The collection contains an unique document. We get it by its id.
        // TODO assert collection contains only one element and get the first
        q.Get(q.Ref(q.Collection('featured_projects'), '293695428824662535'))
      )
  /** @type [string] */
  const featuredProjectsIds = response.data.main
  /** @type {{data: [DbProject]}} */
  const { data: unsortedProjects } =
    await client
      .query(
        q.Map(
          q.Paginate(
            q.Union(
              featuredProjectsIds.map(
                id => q.Match(q.Index('unique_projects_id'), id)
              )
            )
          ),
          ref => q.Get(ref)
        )
      )
  const sortedProjects = featuredProjectsIds.map(
    id => unsortedProjects.find(
      project => project.data.id === id
    )
  )
  // For some reason, the project a5331e21 doesn't show up in unsortedProjects
  // list, which make it undefined in sortedProjects. So we filter this array to
  // be sure there is only valid project.
  //
  // TODO why a5331e21 doesn't show up in unsortedProjects?
  return sortedProjects.filter(project => project !== undefined)
}

/**
 * @param {faunadb.Client} client 
 * @param {string} userId 
 */
const getUserProjects = async function(client, userId) {
  /** @type {faunadb.values.Document<[DbProject]>} */
  const result = await client
    .query(
      q.Map(
        q.Paginate(q.Match(
          q.Index('projects_search_by_user'),
          q.Ref(q.Collection('users'), userId)
        ), { size: 1000 }),
        ref => q.Get(ref)
      )
    )
  return result.data
}

/**
 * @param {faunadb.Client} client 
 * @param {string} userId 
 * @returns {Promise<DbUser>}
 */
 const getDbUser = async function (client, userId) {
  try {
    // First look if the id the personnalId of a user. Note: "username" is the
    // old property name for "personalId" 

    /** @type {DbUser} */
    const result = await client.query(
      q.Get(q.Match(q.Index('unique_users_username'), userId))
    )
    return result
  } catch {
    // Not a username, so treat the id as a FaunaDB id.
    try {
      /** @type {DbUser} */
      const result = await client.query(
        q.Get(q.Ref(q.Collection('users'), userId))
      )
      return result
    } catch {
      // Nothing found at all
      return undefined
    }
  }
}

/**
 * @param {faunadb.Client} client 
 * @param {string} projectId 
 * @returns {Promise<DbProject>}
 */
 const getProject = async function (client, projectId) {
  try {
    /** @type {DbProject} */
    const result = await client.query(
      q.Get(q.Ref(q.Collection('projects'), projectId))
    )
    const data = result.data

    const faunaDbOwner = await client.query(
      q.Get(q.Ref(q.Collection('users'), data.owner.value.id))
    )
    data.ownerData = faunaDbOwner.data
    data.ownerData.name = data.ownerData.user_metadata.full_name
    
    if (data.thrash) {
      // Rename an old mispelled property name
      data.trash = data.thrash
      delete data.thrash
    }
    return result
  } catch {
    return undefined
  }
}

/**
 * @param {faunadb.Client} client 
 * @param {string} id 
 * @param {Project} info 
 */
const submitProjectInfo = async function (client, id, info) {
  /** @type {DbProject} */
  const result = await client.query(
    q.Update(
      q.Ref(q.Collection('projects'), id),
      {
        data: {
          ...info,
          // The following will remove the mispelled field (which is
          // replaced by 'trash').
          thrash: null
        }
      },
    )
  )
  return result
}

/**
 * @param {faunadb.Client} client 
 * @param {Project} project 
 * @returns {Promise<DbProject>}
 */
const createProject = async function (client, project) {
  const me = q.CurrentIdentity()
  /** @type {DbProject} */
  return await client
    .query(
      q.Create(q.Collection('projects'), {
        data: {
          ...project,
          owner: me
        },
        permissions: {
          read: me,
          write: me
        }
      })
    )
}


/**
 * @param {faunadb.Client} client 
 * @param {string} userId 
 * @param {User} changes
 * @returns {Promise<DbUser>}
 */
 const updateUser = async function (client, userId, changes) {
  return await client.query(
    q.Update(
      q.Ref(q.Collection('users'), userId),
      { data: changes },
    )
  )
}

/**
 * @param {faunadb.Client} client 
 * @param {string} projectUniqueId 
 * @returns {Promise<Project>}
 */
const getProjectByUid = async function (client, projectUniqueId) {
  /** @type {DbProject} */
  const result = await client.query(
    q.Get(q.Match(q.Index('unique_projects_id'), projectUniqueId))
  )
  return result.data
}

export default {
  publicClient,
  getClient,
  getCurrentUser,
  getFeaturedProjects,
  getUserProjects,
  getDbUser,
  getProject,
  submitProjectInfo,
  createProject,
  updateUser,
  getProjectByUid
}