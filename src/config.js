const checkEnvVarsDependencies = () => {
  const expectedEnvVars  = [
    'VITE_FAUNA_PUBLIC_KEY',
    'VITE_GOTRUE_API_URL',
    'VITE_ADACRAFT_STORAGE_URL'
  ]
  
  const envVarIsMissing = (varName) => import.meta.env[varName] === undefined
  const missings = expectedEnvVars.filter(envVar => envVarIsMissing(envVar))
  if (missings.length !== 0) {
    console.error(`This project build misses some settings. The following environment variables are missing: ${missings}. Be sure to set them (in .env.local file for example) and rebuild the project.`)
    alert(`There are some missing settings for this build. See the console for more information.`)
  }
}

checkEnvVarsDependencies()

// @ts-ignore
export const faunaPublicKey = import.meta.env.VITE_FAUNA_PUBLIC_KEY
// @ts-ignore
export const goTrueApiUrl = import.meta.env.VITE_GOTRUE_API_URL
// @ts-ignore
export const adacraftStorageUrl = import.meta.env.VITE_ADACRAFT_STORAGE_URL

// Get the public base path. This path can be different between deployment
// environments. We use vite to build the project which provides the base URL in
// BASE_URL env var (see https://vitejs.dev/guide/build.html#public-base-path.)
//
// @ts-ignore
export const baseUrl = import.meta.env.BASE_URL