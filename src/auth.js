import GoTrue from 'gotrue-js'
import { goTrueApiUrl } from './config'

export default new GoTrue({
  APIUrl: goTrueApiUrl,
  audience: '',
  setCookie: true
})