import { reactive, computed, ref, watchEffect } from 'vue'
import auth from './auth.js'
// TODO move the GoTrue types into auth.js
import { User as AuthUser } from 'gotrue-js'
import db from './db'
import { adacraftStorageUrl } from './config.js'

/**
 * @typedef {import('./db').DbUser} DbUser
 * @typedef {import('./db').DbProject} DbProject
 * @typedef {import('./db').User} User
 * @typedef {import('./db').Project} Project
 */

const authentification = reactive({
  loggedIn: false,
  /** @type string */
  user: undefined,
  /** @type object */
  netlifyUser: undefined,
  /** @type {DbUser} */
  faunadbUser: undefined
})


// TODO major refactor of this function and of the type that it returns. Maybe
// this can be replaced by the User type from db.js.
/**
 * @param {DbUser} faunadbUser 
 */
const getUser = function (faunadbUser) {
  const name = faunadbUser.data.user_metadata.full_name
  return {
    name,
    image: faunadbUser.data.avatar || `https://avatars.dicebear.com/api/jdenticon/${name}.svg?w=120`,
    bio: faunadbUser.data.bio || '',
    // "username" is the old property name for "personalId" 
    personalId: faunadbUser.data.username,
    /** @type string */
    // @ts-ignore
    id: faunadbUser.ref.value.id,
    scratchUsername: faunadbUser.data.scratchUsername,
    twitterUsername: faunadbUser.data.twitterUsername,
    instagramUsername: faunadbUser.data.instagramUsername,
    youtubeUsername: faunadbUser.data.youtubeUsername,
    githubUsername: faunadbUser.data.githubUsername,
    email: authentification.netlifyUser ? authentification.netlifyUser.email : undefined
  }
}


const currentUser = computed(
  () => getUser(authentification.faunadbUser)
)

const currentUserId = computed(
  () =>
  /** @type {string} */
  (
    // @ts-ignore
    authentification.faunadbUser?.ref.value.id
  )
)

/**
 * 
 * @param {string} userId 
 * @returns boolean
 */
const idIsCurrentUser = (userId) => (
  authentification.netlifyUser
  && authentification.faunadbUser
  && userId === currentUserId.value
)

const userIsLoggedIn = computed(
  () => (
    authentification.netlifyUser
    && authentification.faunadbUser
  )
)

export const useStore = () => ({
  authentification,
  currentUser,
  currentUserId,
  getUser,
  idIsCurrentUser,
  userIsLoggedIn
})

const logout = async function () {
  authentification.user = undefined
  authentification.netlifyUser = undefined
  authentification.faunadbUser = undefined
  const user = auth.currentUser()
  if (user !== null) {
    await user.logout()
  }
}

const feedbackUrl = computed(() => (
  userIsLoggedIn.value
  ? `https://tally.so/r/w4BGrm?name=${currentUser.value.name}&email=${currentUser.value.email}`
  : `https://tally.so/r/w4BGrm`
))

export const useSession = () => ({
  logout,
  userIsLoggedIn,
  currentUser,
  feedbackUrl
})

export const useCurrentUser = function () {
  return {
    id: computed(() => currentUserId.value)
  }
}

const dbClient = computed(() => {
  if (authentification.netlifyUser !== undefined) {
    return db.getClient(authentification.netlifyUser.app_metadata.db_token)
  } else {
    return db.publicClient
  }
})

// Featured projects is considered stable enough to put it in the global scope.
/** @type {import('vue').Ref<DbProject[]>} */
const featuredProjects = ref([])
watchEffect(async () => {
  featuredProjects.value = await db.getFeaturedProjects()
})

export const useFeaturedProjects = function () {
  return {
    featuredProjects
  }
}

/**
 * @param {string} email 
 * @param {string} password 
 */
const loginWithNetlify = async function (email, password) {
  const response = await auth.login(email, password, true)
  authentification.user = response.user_metadata.full_name
  authentification.netlifyUser = response
}

/**
 * @returns {AuthUser}
 */
const getNetlifyUser = function () {
  return auth.currentUser()
}

/**
 * @param {AuthUser} user
 * @param {string} password 
 */
const changeNetlifyPassword = async function (user, password) {
  const response = await user.update({ password: password })
  authentification.user = response.user_metadata.full_name
  authentification.netlifyUser = response
}

/**
 * 
 * @param {AuthUser} user 
 * @param {object} userInfo 
 */
const submitNetlifyUserInfto = async function (user, userInfo) {
  const response = await user.update(userInfo)
  authentification.user = response.user_metadata.full_name
  authentification.netlifyUser = response
}

const updateCurrentFaunadbUser = async function () {
  const client = db.getClient(authentification.netlifyUser.app_metadata.db_token)
  authentification.faunadbUser = await db.getCurrentUser(client)
}

export const useLoginProcess = function () {
  const loginProcessState = ref('waiting to start')

  /**
   * @param {string} email 
   * @param {string} password 
   */
  const startLoginProcess = async function (email, password) {
    loginProcessState.value = 'wait for request response'
    try {
      await loginWithNetlify(email, password)
      loginProcessState.value = 'user is logged in'
    } catch (error) {
      loginProcessState.value = 'login is refused'
      console.log(`error while login with email ${email}: ` + error)
      return
    }
    await updateCurrentFaunadbUser()
    loginProcessState.value = 'user is connected'
  }

  return {
    startLoginProcess,
    loginProcessState: loginProcessState,
  }
}

export const useChangePasswordProcess = function () {
  const processState = ref('waiting to start')

  /**
   * @param {string} password 
   */
  const startChangePasswordProcess = async function (password) {
    processState.value = 'wait for request response'
    try {
      const user = getNetlifyUser()
      if (user === null) {
        processState.value = 'no current user'
        return
      } else {
        await changeNetlifyPassword(user, password)
      }
    } catch (error) {
      processState.value = 'change is refused'
      console.log(`error while changing password: ` + error)
    }
    await updateCurrentFaunadbUser()
    processState.value = 'user is connected'
  }

  return {
    startChangePasswordProcess,
    processState
  }
}

/**
 * @param {User} user 
 */
export const useUserProjects = function (user) {
  /** @type {import('vue').Ref<DbProject[]>} */
  const projects = ref([])
  watchEffect(async () => {
    const userId = user.id
    if (userId) {
      projects.value = await db.getUserProjects(dbClient.value, userId)
    }
  })

  return {
    projects
  }
}

/**
 * @param {string} userId 
 */
export const useUser = function (userId) {
  const user = reactive({
    id: '',
    data: {}
  })
  const userIsLoading = ref(false)
  watchEffect(async () => {
    userIsLoading.value = true
    const result = await db.getDbUser(dbClient.value, userId)
    userIsLoading.value = false
    if (result) {
      user.data = getUser(result)
      // There is a type complaint about "value" field, which is not declared in
      // the faunadb.Ref type. Ignore the message.
      // @ts-ignore 
      user.id = result.ref.value.id
    }
  })

  return {
    user,
    userIsLoading
  }
}

/**
 * @param {string} projectId 
 */
export const useProject = function (projectId) {
  const project = reactive({
    /** @type {Project} */
    data: {}
  })
  const projectRequestState = ref('pending')
  watchEffect(async () => {
    projectRequestState.value = 'request send'
    const result = await db.getProject(dbClient.value, projectId)
    if (result) {
      project.data = result.data
      projectRequestState.value = 'found'
    } else {
      projectRequestState.value = 'failed'
    }
  })
  return {
    project,
    projectRequestState
  }
}

export const useSubmitProjectInfo = function () {
  /**
   * 
   * @param {string} id 
   * @param {Project} info 
   */
  const submitProjectInfo = async function (id, info) {
    const result = await db.submitProjectInfo(dbClient.value, id, info)
    return result.data
  }

  return {
    submitProjectInfo
  }
}

/**
 * @param {string} storageUrl 
 * @param {string} projectId 
 * @returns 
 */
const fetchProjectFile = async function (storageUrl, projectId) {
  const response = await fetch(
    `${storageUrl}/projects/${projectId}`, {
      "credentials": "omit",
      "headers": {
          "Accept": "*/*",
      },
      "method": "GET",
      "mode": "cors"
    }
  )
  return await response.blob();
}

/**
 * 
 * @param {string} storageUrl 
 * @param {string} projectId 
 * @param {Blob} projectFileContent 
 */
const uploadProjectFile = async function (storageUrl, projectId, projectFileContent) {
  let url = storageUrl
  if (storageUrl === 'https://storage.adacraft.org/2') {
    // This project is stored in the second storage container. For now,
    // we don't use the base URL from the project description but we
    // compute it based on the current configuration of the editor. This
    // allows us to have separate storage spaces for different
    // deployment environment (prod, staging, beta...) even with a
    // shared project database.
    //
    // So, basicly, we convert from something like
    // "https://storage.beta.adacraft.org" to something like
    // "https://storage2.beta.adacraft.org"
    url = adacraftStorageUrl.replace('storage.', 'storage2.')
  }

  const accountToken = authentification.netlifyUser.app_metadata.db_token
  return await fetch(`${url}/projects/${projectId}`, {
      "credentials": "omit",
      "headers": {
          "Accept": "*/*",
          'Account-Token': accountToken,
        },
      "body": projectFileContent,
      "method": "PUT",
      "mode": "cors"
  })
}

export const useSubmitNewProjectProcess = function () {
  const processState = ref('waiting to start')

  /**
   * @param {Project} project 
   * @param {string} sourceStorageBaseUrl 
   * @param {string} sourceProjectId 
   */
  const startSubmitNewProjectProcess = async function (
    project, sourceStorageBaseUrl, sourceProjectId
  ) {
    processState.value = 'wait for request response'

    try {
      /** @type {DbProject} */
      const faunaResponse = await db.createProject(dbClient.value, project)
      const projectId = faunaResponse.ref.id
      const sourceProjectContent = await fetchProjectFile(
        sourceStorageBaseUrl, sourceProjectId
      );
      await uploadProjectFile(project.storage, project.id, sourceProjectContent)
      processState.value = 'project submitted'
      return projectId
    } catch (error) {
      processState.value = 'submission is refused'
      console.log(`error while submitting the new project: ` + error)
    }
  }

  return {
    processState,
    startSubmitNewProjectProcess
  }
}

export const useGetProjectByUid = function () {
  /**
   * @param {string} projectUniqueId 
   */
  const getProjectByUid = async function (projectUniqueId) {
    return db.getProjectByUid(dbClient.value, projectUniqueId)
  }
  return {
    getProjectByUid
  }
}

export const useSubmitUserInfo = function () {
  const processState = ref('wait for user input')
  const changeRequestsError = ref('')

  /**
   * 
   * @param {User} changes 
   */
  const submitUserInfo = async function (changes) {
    try {
      authentification.faunadbUser = await db.updateUser(
        dbClient.value, currentUserId.value, changes
      )
    } catch (error) {
      if (error.message === 'instance not unique') {
        // For now, the only field that has to be unique is the personal ID
        changeRequestsError.value = `The personal ID "${changes.username}" is already used. Please pick another one.`
      } else {
        changeRequestsError.value = error.message
      }
      processState.value = 'changes are refused'
      console.log(error)
      return
    }

    if (changes.user_metadata.full_name) {
      const user = getNetlifyUser()
      try {
        await submitNetlifyUserInfto(
          user,
          {data: { full_name: changes.user_metadata.full_name } }
        )
        processState.value = 'changes applied'
      } catch (error) {
        changeRequestsError.value = error.message
        processState.value = 'changes are refused'
        console.log(error)      
      }
    } else {
      processState.value = 'changes applied'
    }
  }

  return {
    processState,
    changeRequestsError,
    submitUserInfo
  }
}