import { createApp, nextTick } from 'vue'

import App from './App.vue'
import router from './router'
import auth from './auth.js'
import { useStore } from './store.js'
import faunadb from 'faunadb'

import './assets/styles/main.css'

// The recovery token is stored inn the URL as something like that:
//    https://adacraft.org/#recovery_token=sFd-xut8uflMBiXbIrQjTw
const recoveryToken = decodeURIComponent(document.location.hash).split(
  'recovery_token='
)[1]

if (recoveryToken !== undefined) {
  auth.settings
  auth.recover(recoveryToken)
    .then((user) => {
      router.push('change-password')
    })
    .catch((error) => {
      console.log(`Can't recover password, error: ${error}`)
    })
}

const { authentification } = useStore()
const user = auth.currentUser()
if (user) {
  authentification.user = user.user_metadata.full_name
  authentification.netlifyUser = user

  const client = new faunadb.Client({
    secret: authentification.netlifyUser.app_metadata.db_token
  })
  const q = faunadb.query
  client
    .query(q.Identity())
    .then((result) => {
      const userId = result.value.id
      client
        .query(
          q.Get(q.Ref(q.Collection('users'), userId))
        )
        .then((result) => {
          authentification.faunadbUser = result
        })
    })
}


createApp(App)
  .directive('focus', {
    mounted(el, binding, vnode) {
      nextTick(function() {
        el.focus()
      })
    }
  })
  .use(router)
  .mount('#app')
