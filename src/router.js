import { createRouter, createWebHistory } from 'vue-router'

import { baseUrl } from './config'

import ThePageProjects from './components/ThePageProjects.vue'
import ThePageHome from './components/ThePageHome.vue'
import ThePageAbout from './components/ThePageAbout.vue'
import ThePageLogin from './components/ThePageLogin.vue'
import ThePageResetPassword from './components/ThePageResetPassword.vue'
import ThePageChangePassword from './components/ThePageChangePassword.vue'
import ThePageSignUp from './components/ThePageSignUp.vue'
import ThePageProject from './components/ThePageProject.vue'
import ThePageUser from './components/ThePageUser.vue'
import ThePageUserSettings from './components/ThePageUserSettings.vue'
import ThePageDoc from './components/ThePageDoc.vue'
import ThePageProjectAdd from './components/ThePageProjectAdd.vue'
import ThePageHowtoVision from './components/ThePageHowtoVision.vue'
import ThePageHowtoGenerativeArt from './components/ThePageHowtoGenerativeArt.vue'
import ThePageOvhStorageIssue from './components/ThePageOvhStorageIssue.vue'
import ThePageTerms from './components/ThePageTerms.vue'
import ThePageNotFound from './components/ThePageNotFound.vue'

const routerHistory = createWebHistory(baseUrl)

export default createRouter({
  history: routerHistory,
  routes: [
    {
      path: '/',
      component: ThePageHome
    },
    {
      path: '/add-project',
      component: ThePageProjectAdd,
      props: route => ({
        sourceProjectId: route.query.from,
        sourceName: route.query.name ? decodeURI(route.query.name) : undefined,
        sourceDescription: route.query.description ? decodeURI(route.query.description) : undefined,
      })
    },
    {
      path: '/projects',
      component: ThePageProjects
    },
    {
      path: '/docs',
      component: ThePageDoc
    },
    {
      path: '/docs/vision',
      component: ThePageHowtoVision
    },
    {
      path: '/docs/generative-art',
      component: ThePageHowtoGenerativeArt
    },
    {
      path: '/about',
      component: ThePageAbout
    },
    {
      path: '/login',
      component: ThePageLogin
    },
    {
      path: '/reset-password',
      component: ThePageResetPassword
    },
    {
      path: '/change-password',
      component: ThePageChangePassword
    },
    {
      path: '/signup',
      component: ThePageSignUp
    },
    {
      path: '/project/:id',
      component: ThePageProject,
      props: true
    },
    {
      path: '/user/:id',
      component: ThePageUser,
      props: true
    },
    {
      path: '/people/:id',
      component: ThePageUser,
      props: true
    },
    {
      path: '/profile-settings',
      component: ThePageUserSettings
    },
    {
      path: '/ovh-storage-issue',
      component: ThePageOvhStorageIssue
    },
    {
      path: '/terms',
      component: ThePageTerms
    },
    {
      // path: "*",
      path: "/:catchAll(.*)",
      name: "ThePageNotFound",
      component: ThePageNotFound,
    }
  ]
})